package com.kbase.jcv.kpts;

import org.bytedeco.javacpp.opencv_core;

import java.util.List;

/**
 * Created by raman on 6/18/17.
 */
public interface IJCV {
    List<Result> find(List<Context> contextList, opencv_core.IplImage image);
}
