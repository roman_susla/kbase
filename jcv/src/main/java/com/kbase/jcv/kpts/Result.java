package com.kbase.jcv.kpts;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.Getter;

@Getter
public class Result{
    private final String name;
    private final double [] corners;
    private final double square ;
    private final double perimeter ;
    private final double convex ;
    private final double ratio ;


    public Result(String name, double[] corners, double square, double perimeter, double convex) {
        this.corners = corners;
        this.square = square;
        this.perimeter = perimeter;
        this.convex = convex;
        this.name=name;
        this.ratio = square / perimeter;
    }
    public Result(String name)
    {
        this(name,new double[0],0,0,0);
    }
}
