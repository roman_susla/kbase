package com.kbase.jcv.kpts.util;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.logging.Logger;

import static org.bytedeco.javacpp.opencv_calib3d.*;
import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_features2d.*;
import static org.bytedeco.javacpp.opencv_flann.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.*;

import javafx.util.Pair;
import org.bytedeco.javacv.BaseChildSettings;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.OpenCVFrameConverter;

public class ORBObjectFinder {
    public ORBObjectFinder(IplImage objectImage) {
        settings = new Settings();
        settings.objectImage = objectImage;
        setSettings(settings);
    }

    public ORBObjectFinder(Settings settings) {
        setSettings(settings);
    }

    public static class Settings extends BaseChildSettings {
        IplImage objectImage = null;
        ORB detector = ORB.create(500/*=500*/, 1.6f /*=1.2f*/, 6 /*=8*/, 31/*=31*/,
                0/*=0*/, 2/*=2*/, ORB.HARRIS_SCORE/*=cv::ORB::HARRIS_SCORE*/, 31 /*=31*/, 20/*=20*/);

        double distanceThreshold = 0.75;
        int matchesMin = 4;
        double ransacReprojThreshold = 2.0;
        boolean useFLANN = true;

        public IplImage getObjectImage() {
            return objectImage;
        }

        public void setObjectImage(IplImage objectImage) {
            this.objectImage = objectImage;
        }

        public double getDistanceThreshold() {
            return distanceThreshold;
        }

        public void setDistanceThreshold(double distanceThreshold) {
            this.distanceThreshold = distanceThreshold;
        }

        public int getMatchesMin() {
            return matchesMin;
        }

        public void setMatchesMin(int matchesMin) {
            this.matchesMin = matchesMin;
        }

        public double getRansacReprojThreshold() {
            return ransacReprojThreshold;
        }

        public void setRansacReprojThreshold(double ransacReprojThreshold) {
            this.ransacReprojThreshold = ransacReprojThreshold;
        }

        public boolean isUseFLANN() {
            return useFLANN;
        }

        public void setUseFLANN(boolean useFLANN) {
            this.useFLANN = useFLANN;
        }
    }

    int w;
    int h;
    Settings settings;

    public Settings getSettings() {
        return settings;
    }

    public void setSettings(Settings settings) {
        this.settings = settings;
        w = settings.objectImage.width();
        h = settings.objectImage.height();
        Pair<KeyPointVector, Mat> keyPointsAndDescriptors = getKeyPointsAndDescriptors(settings.objectImage, true);
        objectKeypoints = keyPointsAndDescriptors.getKey();
        objectDescriptors = keyPointsAndDescriptors.getValue();

        total = (int) objectKeypoints.size();


    }

    int total = 0;
    static final Logger logger = Logger.getLogger(ORBObjectFinder.class.getName());

    KeyPointVector objectKeypoints = null;
    Mat objectDescriptors = null;

    public Pair<KeyPointVector, Mat> getKeyPointsAndDescriptors(IplImage image, boolean releaseImage) {
        KeyPointVector imageKeypoints = new KeyPointVector();
        Mat imageDescriptors = new Mat();
        settings.detector.detectAndCompute(cvarrToMat(image),
                new Mat(), imageKeypoints, imageDescriptors, false);
        if (releaseImage) cvReleaseImage(image);
        return new Pair<>(imageKeypoints, imageDescriptors);
    }


    public double[] find(IplImage image) {
        Pair<KeyPointVector, Mat> keyPointsAndDescriptors = getKeyPointsAndDescriptors(image, false);
        return find(keyPointsAndDescriptors);
    }

    public double[] find(Pair<KeyPointVector, Mat> keyPointsAndDescriptors) {
        if (objectDescriptors.rows() < settings.getMatchesMin()) {
            return null;
        }
        KeyPointVector imageKeypoints = keyPointsAndDescriptors.getKey();
        Mat imageDescriptors = keyPointsAndDescriptors.getValue();
        if (imageDescriptors.rows() < settings.getMatchesMin()) {
            return null;
        }


        double[] srcCorners = {0, 0, w, 0, w, h, 0, h};
        double[] dstCorners = locatePlanarObject(objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, srcCorners, new ArrayList<Integer>(2 * objectDescriptors.rows()));
        return dstCorners;
    }

    static final int[] bits = new int[256];

    static {
        for (int i = 0; i < bits.length; i++) {
            for (int j = i; j != 0; j >>= 1) {
                bits[i] += j & 0x1;
            }
        }
    }

    int compareDescriptors(ByteBuffer d1, ByteBuffer d2, int best) {
        int totalCost = 0;
        assert d1.limit() - d1.position() == d2.limit() - d2.position();
        while (d1.position() < d1.limit()) {
            totalCost += bits[(d1.get() ^ d2.get()) & 0xFF];
            if (totalCost > best)
                break;
        }
        return totalCost;
    }

    int naiveNearestNeighbor(ByteBuffer vec, ByteBuffer modelDescriptors) {
        int neighbor = -1;
        int d, dist1 = Integer.MAX_VALUE, dist2 = Integer.MAX_VALUE;
        int size = vec.limit() - vec.position();

        for (int i = 0; i * size < modelDescriptors.capacity(); i++) {
            ByteBuffer mvec = (ByteBuffer) modelDescriptors.position(i * size).limit((i + 1) * size);
            d = compareDescriptors((ByteBuffer) vec.reset(), mvec, dist2);
            if (d < dist1) {
                dist2 = dist1;
                dist1 = d;
                neighbor = i;
            } else if (d < dist2) {
                dist2 = d;
            }
        }
        if (dist1 < settings.distanceThreshold * dist2)
            return neighbor;
        return -1;
    }

    void findPairs(Mat objectDescriptors, Mat imageDescriptors, ArrayList<Integer> ptpairs) {
        int size = imageDescriptors.cols();
        ByteBuffer objectBuf = objectDescriptors.createBuffer();
        ByteBuffer imageBuf = imageDescriptors.createBuffer();

        for (int i = 0; i * size < objectBuf.capacity(); i++) {
            ByteBuffer descriptor = (ByteBuffer) objectBuf.position(i * size).limit((i + 1) * size).mark();
            int nearestNeighbor = naiveNearestNeighbor(descriptor, imageBuf);
            if (nearestNeighbor >= 0) {
                ptpairs.add(i);
                ptpairs.add(nearestNeighbor);
            }
        }
    }

    void flannFindPairs(Mat objectDescriptors, Mat imageDescriptors, ArrayList<Integer> ptpairs) {
        int length = objectDescriptors.rows();
        Mat indicesMat, distsMat;

        indicesMat = new Mat(total, 2, CV_32SC1);
        distsMat = new Mat(total, 2, CV_32FC1);
        SearchParams searchParams = new SearchParams(64, 0, true); // maximum number of leafs checked
        IndexParams indexParams = new LshIndexParams(12, 20, 2); // using LSH Hamming distance
        Index flannIndex = new Index();


        // find nearest neighbors using FLANN 
        flannIndex.build(imageDescriptors, indexParams, FLANN_DIST_HAMMING);
        flannIndex.knnSearch(objectDescriptors, indicesMat, distsMat, 2, searchParams);

        IntBuffer indicesBuf = indicesMat.createBuffer();
        IntBuffer distsBuf = distsMat.createBuffer();
        for (int i = 0; i < length; i++) {
            if (distsBuf.get(2 * i) < settings.distanceThreshold * distsBuf.get(2 * i + 1)) {
                ptpairs.add(i);
                ptpairs.add(indicesBuf.get(2 * i));
            }
        }
    }

    /**
     * a rough implementation for object location
     */
    double[] locatePlanarObject(KeyPointVector objectKeypoints, Mat objectDescriptors,
                                KeyPointVector imageKeypoints, Mat imageDescriptors, double[] srcCorners, ArrayList<Integer> ptpairs) {
        ptpairs.clear();
        if (settings.useFLANN) {
            flannFindPairs(objectDescriptors, imageDescriptors, ptpairs);
        } else {
            findPairs(objectDescriptors, imageDescriptors, ptpairs);
        }
        int n = ptpairs.size() / 2;
        if (n < settings.matchesMin) {
            return null;
        }

        Mat pt1 = new Mat(total, 1, CV_32FC2),
                pt2 = new Mat(total, 1, CV_32FC2),
                mask = new Mat(total, 1, CV_8UC1),
                H = new Mat(3, 3, CV_64FC1);

        pt1.resize(n);
        pt2.resize(n);
        mask.resize(n);
        FloatBuffer pt1Idx = pt1.createBuffer();
        FloatBuffer pt2Idx = pt2.createBuffer();
        for (int i = 0; i < n; i++) {
            Point2f p1 = objectKeypoints.get(ptpairs.get(2 * i)).pt();
            pt1Idx.put(2 * i, p1.x());
            pt1Idx.put(2 * i + 1, p1.y());
            Point2f p2 = imageKeypoints.get(ptpairs.get(2 * i + 1)).pt();
            pt2Idx.put(2 * i, p2.x());
            pt2Idx.put(2 * i + 1, p2.y());
        }

        H = findHomography(pt1, pt2, CV_RANSAC, settings.ransacReprojThreshold, mask, 2000, 0.995);
        if (H.empty() || countNonZero(mask) < settings.matchesMin) {
            return null;
        }

        double[] h = (double[]) H.createIndexer(false).array();
        double[] dstCorners = new double[srcCorners.length];
        for (int i = 0; i < srcCorners.length / 2; i++) {
            double x = srcCorners[2 * i], y = srcCorners[2 * i + 1];
            double Z = 1 / (h[6] * x + h[7] * y + h[8]);
            double X = (h[0] * x + h[1] * y + h[2]) * Z;
            double Y = (h[3] * x + h[4] * y + h[5]) * Z;
            dstCorners[2 * i] = X;
            dstCorners[2 * i + 1] = Y;
        }
        return dstCorners;
    }
}
