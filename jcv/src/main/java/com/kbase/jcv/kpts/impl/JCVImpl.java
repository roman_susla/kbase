package com.kbase.jcv.kpts.impl;

import com.kbase.jcv.kpts.Context;
import com.kbase.jcv.kpts.IJCV;
import com.kbase.jcv.kpts.Result;
import javafx.util.Pair;
import org.bytedeco.javacpp.opencv_core;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.bytedeco.javacpp.opencv_core.cvReleaseImage;

/**
 * Created by raman on 6/18/17.
 */
public class JCVImpl implements IJCV {

    public List<Result> find(List<Context> contextList, opencv_core.IplImage image) {
        Pair<opencv_core.KeyPointVector, opencv_core.Mat> pair = contextList.get(0).getFinder().getKeyPointsAndDescriptors(image, false);
        final List<Result> results = new ArrayList<>();
        contextList.stream().parallel().forEach(context ->
        {
            double[] dst = context.getFinder().find(pair);

            if (dst != null && dst.length > 0) {
                List<opencv_core.Point> points = new ArrayList<>(dst.length / 2);
                for (int i = 0; i < dst.length; i += 2) {
                    points.add(new opencv_core.Point((int) dst[i], (int) dst[i + 1]));
                }
                List<Double> sides = sides(points);
                double perimeter = perimeter(sides);
                double square = square(sides);
                int convex = convex(points);
                if (convex > 0) results.add(context.found(dst, square, perimeter, convex));
            }
        });
        Comparator<Result> contextComparator = Comparator.comparingDouble(Result::getRatio);
        results.sort((a, b) -> contextComparator.compare(b, a));
        return results;
    }

    private static List<Double> sides(List<opencv_core.Point> points) {
        List<Double> sides = new ArrayList<>();
        for (int i = 0; i < points.size() - 1; i++) {
            opencv_core.Point p1 = points.get(i);
            opencv_core.Point p2 = points.get(i + 1);
            sides.add(Math.sqrt(Math.pow(p1.x() - p2.x(), 2) + Math.pow(p1.y() - p2.y(), 2)));
        }
        sides.add(Math.sqrt(Math.pow(points.get(points.size() - 1).x() - points.get(0).x(), 2) +
                Math.pow(points.get(points.size() - 1).y() - points.get(0).y(), 2)));
        return sides;
    }

    private static double perimeter(List<Double> sides) {
        return sides.stream().parallel().mapToDouble(d -> d).sum();
    }

    private static double square(List<Double> sides) {
        final double halfPerimeter = perimeter(sides) / 2;
        return Math.sqrt(sides.stream().parallel().mapToDouble(d -> halfPerimeter - d).reduce(1, (a, b) -> a * b));
    }

    private static int convex(List<opencv_core.Point> p) {
        int i, j, k;
        int flag = 0;
        double z;
        int n = p.size();
        if (n < 3)
            return (0);

        for (i = 0; i < n; i++) {
            j = (i + 1) % n;
            k = (i + 2) % n;
            z = (p.get(j).x() - p.get(i).x()) * (p.get(k).y() - p.get(j).y());
            z -= (p.get(j).y() - p.get(i).y()) * (p.get(k).x() - p.get(j).x());
            if (z < 0)
                flag |= 1;
            else if (z > 0)
                flag |= 2;
            if (flag == 3)
                return (-1);
        }
        if (flag != 0)
            return (1);
        else
            return (0);
    }
}
