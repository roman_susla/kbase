package com.kbase.jcv.kpts;

import com.kbase.jcv.kpts.util.ORBObjectFinder;
import lombok.Getter;
import lombok.ToString;
import org.bytedeco.javacpp.opencv_core;

/**
 * Created by raman on 6/18/17.
 */
@ToString
@Getter
public class Context {

    private final String name;
    private final ORBObjectFinder finder;

    Context(String name, opencv_core.IplImage image) {
        this.name = name;
        this.finder = new ORBObjectFinder(image);
    }

    public Result found(double[] corners, double square, double perimeter, int convex) {
        return new Result(name, corners, square, perimeter, convex);
    }
}
