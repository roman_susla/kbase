/*
 * Copyright (C) 2009-2015 Samuel Audet
 *
 * Licensed either under the Apache License, Version 2.0, or (at your option)
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation (subject to the "Classpath" exception),
 * either version 2, or any later version (collectively, the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *     http://www.gnu.org/licenses/
 *     http://www.gnu.org/software/classpath/license.html
 *
 * or as provided in the LICENSE.txt file that accompanied this code.
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 *
 * Adapted from the find_obj.cpp sample in the source package of OpenCV 2.3.1:
 *
 * A Demo to OpenCV Implementation of SURF
 * Further Information Refer to "SURF: Speed-Up Robust Feature"
 * Author: Liu Liu
 * liuliu.1987+opencv@gmail.com
 */

package com.kbase.jcv.kpts;

import com.kbase.jcv.kpts.impl.JCVImpl;
import org.bytedeco.javacpp.opencv_core;

import java.io.File;
import java.util.*;
import java.util.Arrays;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.cvLine;

/**
 *
 */
public class Runner {
    public static void main(String[] args) throws Exception {
        String folderPath = args[0];
        String sceneFolderPath = args[1];
        List<Context> contextList = new ArrayList<>();
        File[] files = new File(folderPath).listFiles();
        for (File file : files) {
            final String path = file.getAbsolutePath();
            IplImage objectImage = cvLoadImage(path, CV_LOAD_IMAGE_GRAYSCALE);
            contextList.add(new Context(file.getName(), objectImage));
        }


        File[] sceneFiles = new File(sceneFolderPath).listFiles();
        assert sceneFiles != null;
        final long start = System.currentTimeMillis();

        Arrays.stream(sceneFiles).forEach((file) -> {
            String scenePath = file.getAbsolutePath();
            if (contextList.size() == 0) {
                System.out.println("No objects found.");
                return;
            }
            IplImage image = cvLoadImage(scenePath, CV_LOAD_IMAGE_GRAYSCALE);
            IJCV jcv = new JCVImpl();


            List<Result> results = jcv.find(contextList, image);

            Result bestCandidate = results.get(0);

            System.out.println("Best candidate:" + bestCandidate.getName() + "  for " + file.getName());
            saveResult(image, "result_for-" + file.getName(), bestCandidate.getCorners());
        });
        System.out.println("Finding time = " + (System.currentTimeMillis() - start) + " ms");

    }

    private static void saveResult(IplImage image, String path, double[] resultCorners) {
        opencv_core.IplImage result = opencv_core.IplImage.createCompatible(image);
        cvCopy(image, result);
        plotResult(result, resultCorners);
        cvSaveImage(path, result);
    }

    private static synchronized void plotResult(IplImage result, double[] dst) {
        if (dst.length != 8) return;
        cvLine(result, cvPoint((int) dst[0], (int) dst[1]), cvPoint((int) dst[2], (int) dst[3]), CV_RGB(255, 0, 0));
        cvLine(result, cvPoint((int) dst[2], (int) dst[3]), cvPoint((int) dst[4], (int) dst[5]), CV_RGB(255, 0, 0));
        cvLine(result, cvPoint((int) dst[4], (int) dst[5]), cvPoint((int) dst[6], (int) dst[7]), CV_RGB(255, 0, 0));
        cvLine(result, cvPoint((int) dst[6], (int) dst[7]), cvPoint((int) dst[0], (int) dst[1]), CV_RGB(255, 0, 0));
    }


}