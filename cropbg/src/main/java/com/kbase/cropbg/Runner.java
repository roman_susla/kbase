package com.kbase.cropbg;

import java.io.File;
import java.util.*;

import static org.bytedeco.javacpp.opencv_core.*;
import static org.bytedeco.javacpp.opencv_imgcodecs.*;
import static org.bytedeco.javacpp.opencv_imgproc.CV_BGR2GRAY;
import static org.bytedeco.javacpp.opencv_imgproc.cvCanny;
import static org.bytedeco.javacpp.opencv_imgproc.cvCvtColor;

/**
 * Created by Raman_Susla1
 * recognition
 * 6/19/2017
 */
public class Runner
{
    public static void main(String[]args){
        String folderPath = args[0];
        String resultDir = args[1];
        File[] files = new File(folderPath).listFiles();
        assert files != null;
        final long start = System.currentTimeMillis();
        java.util.Arrays.stream(files).parallel().forEach(file -> {
            final IplImage image = cvLoadImage(file.getAbsolutePath(), CV_LOAD_IMAGE_GRAYSCALE);
            cvCanny(image, image, 20, 255);
            int max_left=image.width(),max_top=image.height(),max_right=0,max_bottom=0;
            final java.nio.ByteBuffer buffer = image.createBuffer();

            for(int y = 0; y < image.height(); y++) {
                for(int x = 0; x < image.width(); x++) {
                    int index = y * image.widthStep() + x * image.nChannels();

                    int value = buffer.get(index) & 0xFF;

                    if(value>0){
                        if(max_left>x) max_left=x;
                        if(max_top>y) max_top=y;
                        if(max_right<x) max_right=x;
                        if(max_bottom<y) max_bottom=y;
                    }
                }
            }
            final IplImage original = cvLoadImage(file.getAbsolutePath(), CV_LOAD_IMAGE_UNCHANGED);
            cvSetImageROI(original, cvRect(max_left, max_top, max_right-max_left, max_bottom-max_top));
            final IplImage tmp = cvCreateImage(cvGetSize(original), original.depth(), original.nChannels());
            cvCopy(original, tmp, null);
            cvSaveImage(resultDir + "\\"+file.getName().replaceAll("\\.png",".jpeg"), tmp);
            cvReleaseImage(image);
            cvReleaseImage(original);
            cvReleaseImage(tmp);
            System.gc();
        });
        System.out.println("Cropping time = " + (System.currentTimeMillis() - start) + " ms");
    }
}
