package com.kbase.ocr.impl;

import com.kbase.ocr.IOCR;
import com.kbase.ocr.OCRResult;
import org.bytedeco.javacpp.BytePointer;
import org.bytedeco.javacpp.lept;
import org.bytedeco.javacpp.tesseract;

import java.io.File;

import static org.bytedeco.javacpp.lept.pixDestroy;
import static org.bytedeco.javacpp.lept.pixRead;

/**
 * Created by catmo_000 on 5/23/2017.
 */
public class TesseractOCRImpl implements IOCR {
    final tesseract.TessBaseAPI api;

    public TesseractOCRImpl(String language, File tessDataFile) throws Exception {
        if (tessDataFile == null || !tessDataFile.exists()) {
            throw new Exception("TessData dirrectory not found");
        }
        if (language == null || "".equals(language.trim())) {
            throw new Exception("Language is not defined");
        }
        api = new tesseract.TessBaseAPI();
        if (api.Init(tessDataFile.getAbsolutePath(), language) != 0)
            throw new Exception("Could not initialize tesseract");
    }

    private final String ERROR = "";

    public OCRResult getTextByFileName(String filePath) {
        final File imageFile = new File(filePath);
        final String absoluteFilePath = imageFile.getAbsolutePath();
        BytePointer output = null;
        lept.PIX image = null;
        try {
            image = pixRead(absoluteFilePath);
            api.SetImage(image);
            output = api.GetUTF8Text();
            return new OCRResult(output.getString());
        } catch (Throwable throwable) {
            return new OCRResult(ERROR, new OCRResult.Error(OCRResult.Error.Type.PROCESSING, "Error when trying to read text from image", throwable));
        } finally {
            if (output != null) output.deallocate();
            if (image != null) pixDestroy(image);
        }
    }
}
