package com.kbase.ocr;

/**
 * Created by catmo_000 on 5/23/2017.
 */
public interface IOCR {
    OCRResult getTextByFileName(String filePath);
}
