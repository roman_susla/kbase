package com.kbase.ocr;

import lombok.*;

import java.util.List;

/**
 * Created by catmo_000 on 5/24/2017.
 */
@Getter @ToString
public class OCRResult {
    private String text;
    private Error error;

    public OCRResult(String text, Error error) {
        this.text = text;
        this.error = error;
    }

    public OCRResult(String text) {
        this.text = text;
    }

    @Getter @ToString
    public static class Error {
        public Error(Type type, String message, Throwable exception) {
            this.type = type;
            this.message = message;
            this.exception = exception;
        }

        public Error(Type type, String message) {
            this.type = type;
            this.message = message;
        }

        public Error(Type type) {
            this.type = type;
        }

        public enum Type {INIT, PROCESSING}
        private Type type;
        private String message;
        private Throwable exception;
    }
}

