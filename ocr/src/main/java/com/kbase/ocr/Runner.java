package com.kbase.ocr;

import com.kbase.ocr.impl.TesseractOCRImpl;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by catmo_000 on 5/23/2017.
 */
public class Runner {
    public static void main(String[] args) throws Exception {

        String language = args.length == 2 ? args[0] : "eng";
        String sceneFilename  = args.length == 2 ? args[1] : "C:\\Users\\catmo_000\\Pictures\\HjJBh.png";

        final File tessDataFile = Utils.getResourceFile("tessdata");
        IOCR ocr = new TesseractOCRImpl(language, tessDataFile);
        final long startMils = System.currentTimeMillis();
        OCRResult result = ocr.getTextByFileName(sceneFilename);
        final long endMils = System.currentTimeMillis();
        System.out.println("Time range:" + ((endMils - startMils) / 1000) + "sec");
        System.out.println(result);
    }
    public static File getResourceFile(String path) {
        URL url = Runner.class.getClassLoader().getResource(path);
        if (url == null) return null;
        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            return new File(url.getPath());
        }
    }
}
