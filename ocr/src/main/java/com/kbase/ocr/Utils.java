package com.kbase.ocr;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

/**
 * Created by catmo_000 on 5/24/2017.
 */
public class Utils {
    public static File getResourceFile(String path) {
        URL url = Utils.class.getClassLoader().getResource(path);
        if (url == null) return null;
        try {
            return new File(url.toURI());
        } catch (URISyntaxException e) {
            return new File(url.getPath());
        }
    }
}
